const methods = require('./methods')

module.exports = {
  // name: 'registration'
  version: require('./package.json').version,
  manifest: {
    registration: {
      tribe: {
        create: 'async ',
        get: 'async',
        comment: 'async',
        accept: 'async',
        reject: 'async',
        update: 'async',
        list: 'async'
      }
    }
  },
  init (ssb, config) {
    return {
      registration: {
        tribe: methods(ssb)
      }
    }
  }
}
