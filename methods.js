const Crut = require('ssb-crut')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { isCloakedMsg: isGroup } = require('ssb-ref')
const { where, type, toPullStream } = require('ssb-db2/operators')

const spec = require('./spec/registration-group.js')

// NOTE - this was extracted from ssb-tribes applications
// Language of "application" needs to be replacted with "registration" everywhere

module.exports = function Application (ssb) {
  const crut = new Crut(ssb, spec)
  const groupApplicationList = GroupApplicationList(ssb, crut)

  return {
    create (groupId, input = {}, cb) {
      if (typeof input === 'function') {
        cb = input
        input = {}
      }

      if (input.history) delete input.history

      crut.create({
        ...input,
        version: '1',
        groupId
      }, cb)
    },
    get (registrationId, cb) {
      crut.read(registrationId, (err, registration) => {
        if (err) return cb(err)

        // find the latest accept/ reject decisions
        const decisions = registration.history.reduce((acc, h) => {
          if (h.type !== 'decision') return acc

          if (h.body.accepted) acc.accept = h.body
          else acc.reject = h.body

          return acc
        }, { accept: null, reject: null })

        cb(null, {
          id: registrationId,
          groupId: registration.groupId,
          profileId: registration.profileId || null,
          applicantId: registration.originalAuthor,

          answers: registration.answers,
          decision: decisions.accept || decisions.reject || null, // accept > reject > nothng
          history: registration.history,
          recps: registration.recps
        })
      })
    },

    /* update */
    update (registrationId, input, cb) {
      if (input.history) delete input.history

      crut.update(registrationId, input, cb)
    },
    comment (registrationId, comment, cb) {
      crut.update(registrationId, { comment }, cb)
    },
    accept (registrationId, opts = {}, cb) {
      const {
        comment,
        groupIntro = ''
      } = opts

      ssb.registration.tribe.get(registrationId, (err, registration) => {
        if (err) return cb(err)

        const { groupId, applicantId } = registration

        ssb.tribes.invite(groupId, [applicantId], { text: groupIntro }, (err, invite) => {
          if (err) return cb(err)

          const input = {
            decision: {
              accepted: true,
              addMember: invite.key
            }
          }
          if (comment) input.comment = comment

          crut.update(registrationId, input, cb)
        })
      })
    },
    reject (registrationId, opts = {}, cb) {
      const input = {
        decision: { accepted: false }
      }
      if (opts.reason && opts.reason.length) input.comment = opts.reason
      crut.update(registrationId, input, cb)
    },

    list (opts, cb) {
      if (typeof opts === 'function') return groupApplicationList({}, opts)

      groupApplicationList(opts, cb)
    }
  }
}

function GroupApplicationList (server, crut) {
  return function groupApplicationList (opts, cb) {
    //  TODO replace with crut.list and custom filters!
    if (opts.get === true) opts.get = server.registration.tribe.get
    if (opts.accepted !== undefined && !opts.get) opts.get = server.registration.tribe.get

    const optsError = findOptsError(opts)
    if (optsError) return cb(optsError)

    const { groupId, get, accepted } = opts

    pull(
      server.db.query(
        where(type('registration/group')),
        toPullStream()
      ),
      pull.filter(crut.spec.isRoot),
      (groupId)
        ? pull.filter(m => m.value.content.groupId === groupId)
        : null,

      pull.map(m => m.key),

      // (optionally) convert registrationIds into registration records
      (get !== undefined)
        ? pull(
          paraMap(
            (id, cb) => get(id, (err, registration) => {
              if (err) return cb(null, null) // don't choke of failed gets
              return cb(null, registration)
            })
            , 4
          ), // 4 = width of parallel querying
          pull.filter(Boolean) // filter out failed gets
        )
        : null,

      // (optionally) filter registrations by whether accepted
      (accepted !== undefined)
        ? pull.filter(a => {
          if (accepted === null) return a.decision === null // no response
          return a.decision && a.decision.accepted === accepted // boolean
        })
        : null,

      pull.collect((err, data) => {
        cb(err, data)
      })
    )
  }
}

const VALID_ACCEPTED = [undefined, null, true, false]
function findOptsError ({ groupId, get, accepted }) {
  const head = 'registration.tribe.list expected '

  if (groupId && !isGroup(groupId)) {
    return new Error(`${head} "groupId" to be (undefined | GroupId}, got ${groupId}`)
  }
  if (get && typeof get !== 'function') {
    return new Error(`${head} "get" to be (Function), got ${typeof get}`)
  }
  if (accepted !== undefined) {
    if (!VALID_ACCEPTED.includes(accepted)) {
      return new Error(`${head} "accepted" to be (undefined | null | true | false), got ${accepted}`)
    }
    if (typeof get !== 'function') {
      return new Error(`${head} declaring "accepted" requires "get" to be (Function), got ${typeof get}`)
    }
  }

  return null
}
