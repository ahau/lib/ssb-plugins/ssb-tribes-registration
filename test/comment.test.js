const test = require('tape')
const { promisify: p } = require('util')

const { Server, mock } = require('./helpers')

test('registration.tribe.comment', async t => {
  const alice = Server()

  const recps = [mock.feedId(), alice.id]
  const groupId = mock.groupId()

  let id, comment, val
  try {
    id = await p(alice.registration.tribe.create)(groupId, { recps })

    comment = 'oh btw my birth name is john'
    const updateId = await p(alice.registration.tribe.update)(id, { comment })

    val = await p(alice.get)({ id: updateId, private: true })
  } catch (err) {
    t.fail(err)
  }
  t.deepEqual(
    val.content,
    {
      type: 'registration/group',
      comment: { set: comment },
      recps,
      tangles: {
        registration: { root: id, previous: [id] }
      }
    },
    'comment works'
  )

  alice.close()
  t.end()
})
