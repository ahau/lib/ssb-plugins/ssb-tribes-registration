const test = require('tape')
const { promisify: p } = require('util')

const { Server, mock, replicate } = require('./helpers')

test('registration.tribe.get (v1)', async t => {
  const alice = Server()
  const kaitiaki = Server()

  const groupId = mock.groupId()
  const answers = [
    { q: 'what is your favourate pizza flavour', a: 'hawaiian' }
  ]
  const comment = "p.s. I'm also into adding chilli to hawaiin!"
  const profileId = '%FiR41bB1CrsanZA3VgAzoMmHEOl8ZNXWn+GS5vW3E/8=.sha256'
  const recps = [kaitiaki.id, alice.id]

  let id, update1, application
  let t1, t2, t3
  try {
    id = await p(alice.registration.tribe.create)(groupId, { answers, profileId, recps })
    t1 = (await p(alice.get)(id)).timestamp

    update1 = await p(alice.registration.tribe.update)(id, { comment })
    t2 = (await p(alice.get)(update1)).timestamp

    /* kaitiaki approves */
    await p(replicate)({ from: alice, to: kaitiaki })

    const tipId = await p(kaitiaki.registration.tribe.update)(id, {
      decision: { accepted: true },
      comment: 'WELCOME!'
    })
    t3 = (await p(kaitiaki.get)(tipId)).timestamp

    await p(replicate)({ from: kaitiaki, to: alice })

    application = await p(alice.registration.tribe.get)(id)
  } catch (err) {
    t.fail(err)
  }

  const expected = {
    id,
    groupId,
    applicantId: alice.id,
    profileId,
    recps,

    answers: [
      {
        q: 'what is your favourate pizza flavour',
        a: 'hawaiian'
      }
    ],
    decision: { accepted: true },

    // this section was materialised from the other mutable sections
    // using some getTransformation trickery
    history: [
      {
        type: 'answers',
        author: alice.id,
        timestamp: t1,
        body: [
          {
            q: 'what is your favourate pizza flavour',
            a: 'hawaiian'
          }
        ]
      },
      {
        type: 'comment',
        author: alice.id,
        timestamp: t2,
        body: "p.s. I'm also into adding chilli to hawaiin!"
      },
      {
        type: 'comment',
        author: kaitiaki.id,
        timestamp: t3,
        body: 'WELCOME!'
      },
      {
        type: 'decision',
        author: kaitiaki.id,
        timestamp: t3,
        body: { accepted: true }
      }
    ]
  }

  t.deepEqual(application, expected, 'gets application')

  alice.close()
  kaitiaki.close()
  t.end()
})
