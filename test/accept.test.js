const test = require('tape')
const { promisify: p } = require('util')
const pull = require('pull-stream')
const { where, and, author, descending, isDecrypted, toPullStream } = require('ssb-db2/operators')

const { Server, replicate } = require('./helpers')

test('registration.tribe.accept', async t => {
  const kaitiaki = Server()
  const alice = Server()

  const comment = 'So good you made it!'
  const groupIntro = 'Everyone I would like you to welcome John'
  const recps = [kaitiaki.id, alice.id]
  const answers = [
    { q: 'what is your favourate pizza flavour', a: 'hawaiian' }
  ]

  let id
  try {
    const { groupId } = await p(kaitiaki.tribes.create)({})

    id = await p(alice.registration.tribe.create)(groupId, { answers, recps })
    await p(replicate)({ from: alice, to: kaitiaki })

    await p(kaitiaki.registration.tribe.accept)(id, { comment, groupIntro })
  } catch (err) {
    console.error(err)
    return
  }

  const grabTwo = (cb) => {
    pull(
      kaitiaki.db.query(
        where(
          and(
            isDecrypted('box2'),
            author(kaitiaki.id)
          )
        ),
        descending(),
        toPullStream()
      ),
      pull.take(2),
      pull.collect(cb)
    )
  }

  const [acceptMsg, groupAddMsg] = await p(grabTwo)()

  t.equal(groupAddMsg.value.content.type, 'group/add-member', 'publishes a group/add-member msg')
  t.equal(groupAddMsg.value.content.text, groupIntro, 'groupText is published with group/add-member msg')

  t.deepEqual(
    acceptMsg.value.content,
    {
      type: 'registration/group',
      comment: { set: comment },
      decision: {
        set: {
          accepted: true,
          addMember: groupAddMsg.key
        }
      },
      recps,
      tangles: {
        registration: { root: id, previous: [id] }
      }
    },
    'accept message is sent!'
  )

  alice.close()
  kaitiaki.close()
  t.end()
})
