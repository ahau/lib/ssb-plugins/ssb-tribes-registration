const test = require('tape')
const { isMsg } = require('ssb-ref')
const keys = require('ssb-keys')
const { promisify: p } = require('util')
const pull = require('pull-stream')

const { Server: _Server, replicate } = require('./helpers')

// const sleep = async (t) => new Promise(resolve => setTimeout(resolve, t))

const text1 = 'Hello, can I join?'
const text2 = 'Welcome!'
const text3 = 'Welcome for a second time!'

test('registration.tribe.list (v1)', async t => {
  const Server = (opts) => {
    return _Server(opts)
  }
  const timestamp = Date.now()
  const strangerKeys = keys.generate()
  const kaitiakiKeys = keys.generate()

  const strangerOpts = {
    name: 'stranger-test-' + timestamp,
    keys: strangerKeys
  }
  const kaitiakiOpts = {
    name: 'kaitiaki-test-' + timestamp,
    keys: kaitiakiKeys
  }
  let kaitiaki = Server(kaitiakiOpts)
  let stranger = Server(strangerOpts)

  const ogKaitiakiId = kaitiaki.id
  const ogStrangerId = stranger.id

  const name = (id) => {
    switch (id) {
      case kaitiaki.id: return kaitiakiOpts.name
      case stranger.id: return strangerOpts.name
      default: return 'unknown'
    }
  }

  const finish = (err) => {
    kaitiaki.close()
    stranger.close()
    if (err) console.log('saw error', err)
    t.error(err, 'saw no errors')
    t.end()
  }

  try {
    /* Kaitiaki creates many tribes */
    const groupIds = await pull(
      pull.values(new Array(4)),
      pull.asyncMap((_, cb) => kaitiaki.tribes.create({}, cb)),
      pull.map(g => g.groupId),
      pull.collectAsPromise()
    )

    const [groupId, groupId2, groupId3] = groupIds

    await replicate({ from: kaitiaki, to: stranger, name })

    /* User lists tribes it's part of */
    const initialList = await p(stranger.tribes.list)()
    t.equal(initialList.length, 0, 'stranger sees no applications')

    /* Stranger creates an application to join 3 tribes */
    const applications = await pull(
      pull.values([groupId, groupId2, groupId3]),
      pull.asyncMap((groupId, cb) => setTimeout(cb, 10, null, groupId)),
      // NOTE WARNING - without this (or if using Promise.all) we would see that
      // the strange writes 3 messages, but createHistory stream would return
      // them OUT OF ORDER, meaning our scuttle-testbot replicate function would
      // choke when it tried to add them D:
      // It seems like there is some batch-adding problem... :fire:
      pull.asyncMap((groupId, cb) => {
        stranger.registration.tribe.create(groupId, { comment: text1 }, cb)
      }),
      pull.collectAsPromise()
    )

    t.true(
      applications.every(a => isMsg(a)),
      'stranger makes some applications'
    )

    await replicate({ from: stranger, to: kaitiaki, name })

    /* Kaitiaki lists applications for a tribe */
    let listData = await p(kaitiaki.registration.tribe.list)({
      groupId,
      get: true,
      accepted: null // unresponded
    })

    let application = await p(stranger.registration.tribe.get)(applications[0])
    t.deepEqual(listData, [application], 'kaitiaki can see same application')

    const listData2 = await p(stranger.registration.tribe.list)({})

    /* Stranger closes + restarts server */
    await p(setTimeout)(2000) // HACK unclear why needed
    await p(stranger.close)(true)
    await p(kaitiaki.close)(true)

    await p(setTimeout)(1000)

    stranger = Server({ ...strangerOpts, startUnclean: true })
    kaitiaki = Server({ ...kaitiakiOpts, startUnclean: true })
    // have to restart replication after closing server

    t.equals(stranger.id, ogStrangerId, 'stranger has same id')
    t.equals(kaitiaki.id, ogKaitiakiId, 'stranger has same id')

    /* Stranger checks list of applications */
    const listData3 = await p(stranger.registration.tribe.list)({})
    t.deepEqual(listData2, listData3, 'stranger list same after restart')

    /* Kaitiaki accepts the application */

    await p(kaitiaki.registration.tribe.accept)(
      listData[0].id,
      { comment: text2 }
    )

    await replicate({ from: kaitiaki, to: stranger, name })

    /* Stranger checks the current application state */
    // takes a moment for the stranger to process new membership
    let isReady = false
    while (!isReady) {
      application = await p(stranger.registration.tribe.get)(application.id)
      isReady = application && application.history && application.history.length > 1

      if (!isReady) await wait(500)
    }

    t.deepEqual(
      application.history[1].body,
      text2,
      'stranger can see comment from kaitiaki'
    )
    t.true(isMsg(application.history[2].body.addMember), 'stranger can see group/add-member')

    /* User can now publish to group */
    // takes a moment for group/add-member to be read, keystore to be updated
    isReady = false
    while (!isReady) {
      const groups = await p(stranger.tribes.list)()
      isReady = groups.length

      if (!isReady) await wait(500)
    }
    const published = await p(stranger.tribes.publish)({ type: 'hooray', recps: [groupId] })
    t.true(published, 'stranger can now publish to group')

    await replicate({ from: stranger, to: kaitiaki, name })

    /* Duplicate acceptance */
    await p(kaitiaki.registration.tribe.accept)(
      listData[0].id,
      { comment: text3 }
    )

    await replicate({ from: kaitiaki, to: stranger, name })

    isReady = false
    while (!isReady) {
      application = await p(stranger.registration.tribe.get)(application.id)
      isReady = application && application.history && application.history.length > 3

      if (!isReady) await wait(500)
    }

    t.deepEqual(
      application.history.map(h => {
        const _h = { author: h.author, body: h.body }
        delete h.body.addMember
        // just prune these links off as too hard / not relevant (and tested in accept)
        return _h
      }),
      [
        { author: stranger.id, body: text1 },
        { author: kaitiaki.id, body: text2 },
        { author: kaitiaki.id, body: { accepted: true } },
        { author: kaitiaki.id, body: text3 },
        { author: kaitiaki.id, body: { accepted: true } }
      ],
      'stranger sees all comments'
    )
    // This is really just testing READ, can delete
    // but it does test duplicate accept

    await p(kaitiaki.registration.tribe.reject)(applications[0], {}) // already approved! rejection should do nothing
    await p(kaitiaki.registration.tribe.reject)(applications[1], {})

    await replicate({ from: kaitiaki, to: stranger, name })

    listData = await p(kaitiaki.registration.tribe.list)({
      accepted: true // accepted
    })
    t.equal(listData.length, 1, 'kaitiaki sees 1 accepted applications')

    listData = await p(kaitiaki.registration.tribe.list)({
      accepted: false // rejected
    })
    t.equal(listData.length, 1, 'kaitiaki sees 1 rejected applications')

    listData = await p(kaitiaki.registration.tribe.list)({
      accepted: null // unresponded
    })
    t.equal(listData.length, 1, 'kaitiaki sees 1 applications with no decision')

    finish()
  } catch (err) {
    finish(err)
  }
})

function wait (time) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), time)
  })
}
