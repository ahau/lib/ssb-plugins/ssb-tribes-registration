const test = require('tape')
const { promisify: p } = require('util')

const { Server, replicate } = require('./helpers')

test('registration.tribe.reject', async t => {
  const alice = Server()
  const kaitiaki = Server()
  const { groupId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  const answers = [
    { q: 'what is your favourate pizza flavour', a: 'hawaiian' }
  ]

  let id, reason, rejectId, val
  const recps = [poBoxId, alice.id]
  try {
    id = await p(alice.registration.tribe.create)(groupId, { answers, recps })
    await p(replicate)({ from: alice, to: kaitiaki })

    reason = 'hey this group is no longer accepting new people'
    rejectId = await p(kaitiaki.registration.tribe.reject)(id, { reason })

    val = await p(kaitiaki.get)({ id: rejectId, private: true })
  } catch (err) {
    t.fail(err)
  }

  t.deepEqual(
    val.content,
    {
      type: 'registration/group',
      comment: { set: reason },
      decision: { set: { accepted: false } },
      tangles: {
        registration: { root: id, previous: [id] }
      },
      recps
    },
    'reject works'
  )

  // TODO - reject if there's already a known accept?

  alice.close()
  kaitiaki.close()
  t.end()
})
