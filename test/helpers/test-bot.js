const Server = require('scuttle-testbot')

module.exports = function TestBot (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  //
  //
  //   recpsGuard: false
  // }

  const stack = Server // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-box2'))
    .use(require('ssb-tribes'))
    .use(require('../..'))

  if (opts.recpsGuard) stack.use(require('ssb-recps-guard'))

  const ssb = stack({
    noDefaultUse: true,
    ...opts,
    box2: {
      ...opts.box2,
      legacyMode: true
    }
  })

  return ssb
}
